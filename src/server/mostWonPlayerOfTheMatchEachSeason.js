const mostWonPlayerOfTheMatchEachSeason = (matches) => {
    const result = {};
    const seasons = matches.map((match) => match.season)
        .filter((season, index, matchSeasons) => {
            return matchSeasons.indexOf(season) === index;
        }).sort();

    for (let season of seasons) {
        result[season] = getPlayerOfTheMatch(matches, season);
    }
    return result;
}

const getPlayerOfTheMatch = (matches, season) => {
    const allPlayerOfTheMatch = {};
    for (let match of matches) {
        let player = match.player_of_match;
        if (season === match.season && player !== '')
            allPlayerOfTheMatch[player] = (allPlayerOfTheMatch[player] || 0) + 1;
    }
    let mostWonPlayerOfTheMatch = [];
    for (let player in allPlayerOfTheMatch) {
        mostWonPlayerOfTheMatch.push({
            player,
            "pom": allPlayerOfTheMatch[player],
        });
    }
    mostWonPlayerOfTheMatch.sort((a, b) => {
        return b.pom - a.pom;
    })

    return mostWonPlayerOfTheMatch[0];
};

module.exports = mostWonPlayerOfTheMatchEachSeason;
