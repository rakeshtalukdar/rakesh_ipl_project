const strikeRateOfABatsmanEachSeason = (matches, deliveries, batsman) => {
    const result = {};
    const seasonStrikeRate = {};
    const seasons = matches.map(match => match.season).filter((season, index, matchSeasons) => {
        return matchSeasons.indexOf(season) === index;
    }).sort();

    for (let season of seasons) {
        seasonStrikeRate[season] = getStrikeRate(matches, deliveries, season, batsman);
    }

    result[batsman] = seasonStrikeRate;
    return result;
}

const getStrikeRate = (matches, deliveries, season, batsman) => {
    const runsScored = {};
    const ballFaced = {};

    for (let match of matches) {
        if (season === match.season) {
            for (let delivery of deliveries) {
                if (match.id === delivery.match_id && batsman === delivery.batsman) {
                    let runs = Number(delivery.batsman_runs);
                    runsScored[batsman] = (runsScored[batsman] || 0) + runs;

                    //Wide ball is not counted in batsman's total balls, so this if conditon will skip the count
                    if (Number(delivery.wide_runs) === 0) {
                        ballFaced[batsman] = (ballFaced[batsman] || 0) + 1;
                    }
                }
            }
        }
    }

    let strikeRate = 0;
    for (let batsman in runsScored) {
        strikeRate = ((parseFloat(runsScored[batsman]) / parseFloat(ballFaced[batsman])) * 100).toFixed(2);
    }
    return strikeRate;
};


module.exports = strikeRateOfABatsmanEachSeason;
