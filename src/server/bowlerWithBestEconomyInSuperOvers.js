const bowlerWithBestEconomyInSuperOvers = (deliveries) => {
    const concededRuns = {};
    const totalBalls = {};

    for (let delivery of deliveries) {
        if (delivery.is_super_over !== '0') {

            let runs = parseInt(delivery.total_runs) - (parseInt(delivery.legbye_runs) + parseInt(delivery.penalty_runs) + parseInt(delivery.bye_runs));
            concededRuns[delivery.bowler] = (concededRuns[delivery.bowler] || 0) + runs;
            //Counting ball is skiiped if that ball is wide ball or no ball
            let skipBallCount = parseInt(delivery.wide_runs) + parseInt(delivery.noball_runs);
            if (skipBallCount === 0) {
                totalBalls[delivery.bowler] = (totalBalls[delivery.bowler] || 0) + 1;
            }
        }
    }
    const bowlersWithEconomy = [];
    for (let bowler in concededRuns) {

        //6 balls equals to 1 over, so, ((concededRuns / Total balls) * 6) gives the same result as (runs / overs) formula 
        let economy = ((parseFloat(concededRuns[bowler]) / parseFloat(totalBalls[bowler])) * 6).toFixed(2);
        bowlersWithEconomy.push({
            bowler,
            economy,
        });
    }
    bowlersWithEconomy.sort((a, b) => {
        return a.economy - b.economy;
    });

    const bestEconomicalBowlerInSuperOver = bowlersWithEconomy.slice(0, 1)[0];
    return bestEconomicalBowlerInSuperOver;
};

module.exports = bowlerWithBestEconomyInSuperOvers;
