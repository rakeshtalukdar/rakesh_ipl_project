const mostDismissalOfOnePlayerByAnotherPlayer = (deliveries, batsman) => {
    const result = {};
    const bowlers = {};

    for (let delivery of deliveries) {
        if (delivery.player_dismissed === batsman) {
            bowlers[delivery.bowler] = (bowlers[delivery.bowler] || 0) + 1;
        }
    }
    const mostDismissalBowler = [];
    for (let bowler in bowlers) {
        mostDismissalBowler.push({
            bowler,
            "dismissed": bowlers[bowler],
        })
    }
    mostDismissalBowler.sort((a, b) => {
        return b.dismissed - a.dismissed;
    });

    result[batsman] = mostDismissalBowler[0];
    return result;
}

module.exports = mostDismissalOfOnePlayerByAnotherPlayer;
