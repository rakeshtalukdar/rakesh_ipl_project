const fs = require("fs");
const csvtojson = require("csvtojson");

const teamWonTossAndMatch = require("./teamWonTossAndMatch");
const bowlerWithBestEconomyInSuperOvers = require("./bowlerWithBestEconomyInSuperOvers");
const strikeRateOfABatsmanEachSeason = require("./strikeRateOfABatsmanEachSeason");
const mostWonPlayerOfTheMatchEachSeason = require("./mostWonPlayerOfTheMatchEachSeason")
const mostDismissalOfOnePlayerByAnotherPlayer = require("./mostDismissalOfOnePlayerByAnotherPlayer");

const MATCHES_FILE_PATH = ("../data/matches.csv");
const DELIVERIES_FILE_PATH = ("../data/deliveries.csv");
const JSON_OUTPUT_TEAM_WON_TOSS_AND_MATCH = ("../output/teamWonTossAndMatch.json");
const JSON_OUTPUT_BOWLER_WITH_BEST_ECONOMY_IN_SUPER_OVERS = ("../output/bowlerWithBestEconomyInSuperOvers.json");
const JSON_OUTPUT_STRIKE_RATE_OF_A_BATSMAN_EACH_SEASON = ("../output/strikeRateOfABatsmanEachSeason.json");
const JSON_OUTPUT_MOST_WON_PLAYER_OF_THE_MATCH_EACH_SEASON = ("../output/mostWonPlayerOfTheMatchEachSeason.json");
const JSON_OUTPUT_MOST_DISMISSAL_OF_ONE_PLAYER_BY_ANOTHER_PLAYER = ("../output/mostDismissalOfOnePlayerByAnotherPlayer.json");


const passCsvDataAndGetResults = (() => {
    csvtojson()
        .fromFile(MATCHES_FILE_PATH)
        .then(matches => {
            csvtojson()
                .fromFile(DELIVERIES_FILE_PATH)
                .then(deliveries => {

                    const teamWonTossAndMatchResult = teamWonTossAndMatch(matches);
                    const bowlerWithBestEconomyInSuperOversResult = bowlerWithBestEconomyInSuperOvers(deliveries);
                    const strikeRateOfABatsmanEachSeasonResult = strikeRateOfABatsmanEachSeason(matches, deliveries, "KD Karthik");
                    const mostWonPlayerOfTheMatchEachSeasonResult = mostWonPlayerOfTheMatchEachSeason(matches);
                    const mostDismissalOfOnePlayerByAnotherPlayerResult = mostDismissalOfOnePlayerByAnotherPlayer(deliveries, "MS Dhoni");

                    let errorMessage = "Oops!! An error occured while saving results.";

                    saveResultsInJsonFile(teamWonTossAndMatchResult, JSON_OUTPUT_TEAM_WON_TOSS_AND_MATCH)
                        .then((data) => data)
                        .catch((error) => console.error(`${errorMessage} ${error}`));

                    saveResultsInJsonFile(bowlerWithBestEconomyInSuperOversResult, JSON_OUTPUT_BOWLER_WITH_BEST_ECONOMY_IN_SUPER_OVERS)
                        .then((data) => data)
                        .catch((error) => console.error(`${errorMessage} ${error}`));

                    saveResultsInJsonFile(strikeRateOfABatsmanEachSeasonResult, JSON_OUTPUT_STRIKE_RATE_OF_A_BATSMAN_EACH_SEASON)
                        .then((data) => data)
                        .catch((error) => console.error(`${errorMessage} ${error}`));

                    saveResultsInJsonFile(mostWonPlayerOfTheMatchEachSeasonResult, JSON_OUTPUT_MOST_WON_PLAYER_OF_THE_MATCH_EACH_SEASON)
                        .then((data) => data)
                        .catch((error) => console.error(`${errorMessage} ${error}`));

                    saveResultsInJsonFile(mostDismissalOfOnePlayerByAnotherPlayerResult, JSON_OUTPUT_MOST_DISMISSAL_OF_ONE_PLAYER_BY_ANOTHER_PLAYER)
                        .then((data) => data)
                        .catch((error) => console.error(`${errorMessage} ${error}`));
                });
        });
})();

const saveResultsInJsonFile = (result, jsonFilePath) => {
    const jsonData = JSON.stringify(result);
    return new Promise((resolve, reject) => {
        fs.writeFile(jsonFilePath, jsonData, "utf8", (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(jsonData);
            }
        });
    });
};
