const teamWonTossAndMatch = (matches) => {
    const result = {};
    for (let match of matches) {
        let team = match.winner;
        if (match.toss_winner === team) {
            result[team] = (result[team] || 0) + 1;
        }
    }
    return result;
};

module.exports = teamWonTossAndMatch;
